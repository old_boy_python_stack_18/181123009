from django.shortcuts import render
from django.http.response import JsonResponse
import time
from web.forms import UserProfileForm
# Create your views here.
from web import models


def home(request):
    return render(request, "home.html")


def home2(request):
    if request.method == "POST":
        time.sleep(2)
        print(request.POST)
        return JsonResponse({"status": 0, "msg": 'ok'})
    return render(request, "home2.html")


def user_list(request):
    user_all = models.UserProfile.objects.all()
    return render(request, "user_list.html", {'user_all': user_all})


def user_add(request, endit_id=None):
    # obj = models.User.objects.filter(pk=endit_id).first()
    if request.method == 'POST':
        user_info = request.POST.dict()
        print(user_info)
        del user_info["csrfmiddlewaretoken"]
        models.UserProfile.objects.create(**user_info)
        return JsonResponse({"status": 0, "msg": 'ok'})
    else:
        form_obj = UserProfileForm()
    return render(request, 'user_add.html', {'form_obj': form_obj})


def user_edit(request, endit_id=None):
    obj = models.UserProfile.objects.filter(pk=endit_id).first()
    if request.method == 'POST':
        user_info = request.POST.dict()
        print(user_info)
        del user_info["csrfmiddlewaretoken"]
        if endit_id:
            models.UserProfile.objects.filter(pk=endit_id).update(**user_info)
            return JsonResponse({"status": 0, "msg": 'ok'})
    else:
        form_obj = UserProfileForm(instance=obj)
    return render(request, 'user_edit.html', {'form_obj': form_obj,'uid':endit_id})

def user_dell(request,endit_id=None):
    models.UserProfile.objects.get(pk=endit_id).delete()
    return  redirect(reverse('user_list'))

from django.shortcuts import render, HttpResponse, redirect, reverse
from django.http import JsonResponse
import time
# Create your views here.

from .models import UserProfile


def login(request):
    error_msg = ''
    if request.method == "POST":
        email = request.POST.get('email')
        password = request.POST.get('password')
        home = UserProfile.objects.filter(email=email, password=password).first()
        if home:
            request.session["user_id"] = home.pk
            return redirect(reverse('userall'))
        error_msg = "用户名或密码错误"
    return render(request, 'login.html', {"error_msg": error_msg})


def logout_view(request):
    request.session.flush()
    return redirect(reverse('login'))

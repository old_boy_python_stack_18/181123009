"""issue URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import home
    2. Add a URL to urlpatterns:  url(r'^$', home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from web.views import *

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^home_list/', home, name='home'),
    url(r'^home/', home, name='home'),
    url(r'^home2/', home2, name='home2'),
    url(r'^user_list/', user_list, name='user_list'),
    url(r'^user_add/', user_add, name='user_add'),
    url(r'^user_edit/(\d+)', user_edit, name='user_edit'),
    url(r'^user_dell/(\d+)', user_dell, name='user_dell'),
]
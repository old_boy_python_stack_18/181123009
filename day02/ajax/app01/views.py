from django.shortcuts import render, HttpResponse
from app01 import models
import json
from django.http import JsonResponse


def index(request):
    return render(request, 'index2.html')
def test(request):
    return  render(request,'test.html')

def login(request):
    user = request.POST.get("user")
    pwd = request.POST.get("pwd")
    # 根据表单的用户名和密码到数据库中匹配
    user_obj = models.User.objects.filter(username=user, password=pwd).first()
    print(user_obj)
    # 一般请求下，需要定义一个字典。msg是约定成俗的名字,用来做提示的
    response = {"user": None, "msg": None}
    if user_obj:  # 判断有返回结果的请求下
        response["user"] = user_obj.username  # 修改字典的用户名
        print(user_obj.username)
    else:
        response["msg"] = "用户名或者密码不一致"  # 修改提示信息
    # 返回json格式数据,默认序列化时,对中文默认使用的ascii编码。
    # ensure_ascii=False表示显示真正的中文
    # return HttpResponse(json.dumps(response, ensure_ascii=False))
    return JsonResponse(response)


def file_put(request):
    if request.method == 'POST':
        print(request.POST)  # 打印post信息
        print(request.FILES)  # 打印文件信息
        file_obj = request.FILES.get("img")
        print(type(file_obj))
        # print(file_obj.__dict__)  # 打印img对象属性
        print(file_obj.name)  # 打印文件名
        response = {"state": False}
        with open("statics/img/" + file_obj.name, 'wb') as f:
            for line in file_obj:
                ret = f.write(line)  # 写入文件
                print(ret)  # 返回写入
                if ret:  # 判断返回值
                    response["state"] = True
        return HttpResponse(json.dumps(response))
    return render(request, "file_put.html")  # 渲染file_put.html


def ajax_handle(request):
    print(request.POST)
    print(request.body)
    data = json.loads(request.body.decode('utf-8'))
    print(data)
    print(data["a"])
    return HttpResponse('ok')

# -*- coding: utf-8 -*-
# Generated by Django 1.11.14 on 2019-03-05 13:47
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app01', '0002_auto_20190305_2110'),
    ]

    operations = [
        migrations.RenameField(
            model_name='user',
            old_name='pwd',
            new_name='password',
        ),
        migrations.RenameField(
            model_name='user',
            old_name='user',
            new_name='username',
        ),
    ]

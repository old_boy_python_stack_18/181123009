from s1 import  myfun1,c
from celery.result import AsyncResult
from datetime import timedelta
#指定多长时间以后执行
# s=myfun1.apply_async((10,20),countdown=5)
#第二种 知道就行
s=myfun1.apply_async((10,20),eta="utc")
print(s.id)
#还有延时 重试等党发
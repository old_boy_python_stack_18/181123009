#生产者
from s1 import myfun1,myfun2,myfun3,c
from  celery.result import AsyncResult
s=myfun1.delay(10,20)#生产
# print(s.id)#拿id
# print(s.status)#等待返回PENDING
r=AsyncResult(id=s.id,app=c)
# print(r.successful())#False
print(r.get())#拿到返回值
print(r.get(propagate=False))#只获取报错信息
print(r.traceback)#获取具体出错位置
# print(r.status)#SUCCESS
# print(r.successful())#True



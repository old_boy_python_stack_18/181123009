from s1 import c
from celery.beat import crontab
c.conf.beat_schedule = {
    # "name": {
    #     "task": "s1.myfun1",#执行谁的
    #     "schedule": 3,#间隔时间
    #     "args": (10, 20)#传的参数
    # },#第二种方式
    "crontab": {
        "task": "s1.myfun1",
        "schedule": crontab(minute=53),#指定到多少秒执行
        "args": (10, 20)
    }
}

from django.db import models

# Create your models here.
class User(models.Model):
    '''
    用户信息
    '''
    name= models.CharField('用户名',max_length=64,unique=True)
    password =models.CharField('密码',max_length=64)
from django.shortcuts import render,redirect,reverse,HttpResponse
from app01 import models
from app01.forms import User1
# Create your views here.
def index(request):
    return  render(request,'index.html')

def login(request):
    if request.method=='POST':
        user=request.POST.get('username')
        pwd=request.POST.get('password')
        obj=models.User.objects.filter(name=user,password=pwd).first()
        if obj:
            #登陆成功
            return redirect(reverse('index'))
        else:
            #登陆失败
            return  render(request,'login.html',{'error1':'用户密码错误'})
    return render(request,'login.html')

def user_list(request):
    user_all=models.User.objects.all()
    return render(request,'user_list.html',{'user_all':user_all})

def user_cheng(request,endit_id=None):
    obj=models.User.objects.filter(pk=endit_id).first()
    if request.method=='POST':
        form_obj=User1(data=request.POST,instance=obj)
        if form_obj.is_valid():
            form_obj.save()
            return redirect(reverse('user_list'))
    else:
        form_obj = User1(instance=obj)
    return render(request,'user_cheng.html',{'form_obj':form_obj})

def user_del(requset,n):
    models.User.objects.get(pk=n).delete()
    return redirect(reverse('user_list'))
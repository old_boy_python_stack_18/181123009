from django import forms
from app01 import models
# from django.core.exceptions import NON_FIELD_ERRORS, ValidationError

class BSForm(forms.ModelForm):
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        #自定义操作
        for fied in self.fields.values():
            fied.widget.attrs.update({'class': 'form-control'})

class User1(BSForm):
    class Meta:
        model = models.User
        fields = '__all__'
        # exclude = ['password']



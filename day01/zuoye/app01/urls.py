"""zuoye URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from app01 import views
urlpatterns = [
    url(r'^login/', views.login,name='login'),
    url(r'^index/', views.index, name='index'),
    url(r'^user_list/',views.user_list,name='user_list'),
    url(r'^user_edit/(\d+)/',views.user_cheng,name='user_edit'),
    url(r'^user_add/',views.user_cheng,name='user_add'),
    url(r'^user_del/(\d+)/',views.user_del,name='user_del')
]

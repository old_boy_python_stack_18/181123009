#原理 实例一个etree对象,将页面源码加载改对象中
#使用etrr中的xpath方法结合xpanth表达式进行标签定位和数据提取
#实例化对象
#etree.parse('本地文件路径')
#etrss.Html(page_text)#远程文件
from lxml import etree
tree=etree.parse('./test.html')

#定位title标签
# print(tree.xpath('/html/head/title/text()'))#查找title
# print(tree.xpath('/html//title'))
# print(tree.xpath('//title/text()'))
#定位class
print(tree.xpath('//div[@class="song"]/p[1]/text()')[0])
print(tree.xpath('//div[@class="tang"]/ul/li[4]/a/text()'))
#取属性
print(tree.xpath('//a/@title'))#找到所有的title属性 遇到属性取属性

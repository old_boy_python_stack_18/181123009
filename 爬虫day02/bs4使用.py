#2种 方式
#BeautifulSoup(page_test,'lmxl')#从互联网请求到数据源码加载到对象中
#BeautifulSoup(fb,'lmxl')#将本地源码加载在对象中
from bs4 import BeautifulSoup
fp = open('./test.html','r',encoding='utf-8')
soup=BeautifulSoup(fp,"html5lib")
# print(soup.title)
# print(soup.div)#默认会找第一个div
#
# print(soup.find('a'))#查询a 默认第一个
# #属性定位
# print(soup.find('div',class_='song'))

# print(soup.find_all('div')[2])#查找所有div 并找出第二个div 从0开始的

#select(选择器)
# print(soup.select('.song'))
# print(soup.select('div'))#变成一个list
#层级
# >表示一个层级  空格表示多个层级
# print(soup.select('.tang > ul > li >a'))#取出所有的a
# print(soup.select('.tang a'))#取出所有的a

#取出直系文本数据 text获取全部的数据
# print(soup.p.string)
# print(soup.find('div',class_='tang').get_text())
# print(soup.find('div',class_='tang').text)
#取属性
# print(soup.a['href'])
# print(soup.select('.tang>ul>li>a')[0]['href'])

from lxml import etree
import requests
import json

headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36"
}
session = requests.Session()
url = 'https://apimobile.meituan.com/group/v4/poi/pcsearch/1?'
param = {
    "uuid": "7cafd1fbd4e8408fae24.1559131838.1.0.0",
    "userid": "-1",
    "limit": "32",
    "offset": "32",
    "cateId": "-1",
    "q": "自助餐"
}

lists = []
# # 访问的时候带了cookie
s = requests.Session()
page_text = s.get(url=url, headers=headers, params=param).json()
title_list = page_text['data']["searchResult"]

def shop_intfo(id, f):
    originUrl = 'https://www.meituan.com/meishi/' + str(id)
    param2 = {
        "uuid": "7cafd1fbd4e8408fae24.1559131838.1.0.0",
        "platform": "1",
        "partner": "126",
        "originUrl": 'originUrl',
        "riskLevel": "1",
        "optimusCode": "1",
        "id": id,
        "userId": "",
        "offset": "0",
        "pageSize": "10",
        "sortType": "1",
    }
    page_text = s.get(url=tai_url, headers=headers, params=param2).json()
    pinglu_list = page_text['data']['comments']
    if pinglu_list:
        for i in pinglu_list:
            f.write(i['userName'] + '\n')
            f.write(i['comment'] + '\n')
    else:
        f.write('无评论\n')

for i in title_list:
    # i["title"], i["avgscore"], i["address"]
    f = open("./meituan/" + i['title'] + '.txt', 'a', encoding='utf-8')
    tai_url = 'https://www.meituan.com/meishi/api/poi/getMerchantComment?'
    shop_intfo(i['id'], f)
    f.flush()
    f.close()



# 使用线程池爬取视频中的短视频
from lxml import etree

import requests
import random
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36'
}
url = 'https://www.pearvideo.com/category_1'
page_text = requests.get(url=url, headers=headers).text

from multiprocessing.dummy import Pool
pool = Pool(4)
viseo_urls = []  # 所有视频的url
tree = etree.HTML(page_text)

# 解析视频详情url
li_list = tree.xpath('//*[@id="listvideoListUl"]/li')
'''
var contId="1559965",liveStatusUrl="liveStatus.jsp",liveSta="",playSta="1",autoPlay=!1,isLiving=!1,isVrVideo=!1,hdflvUrl="",sdflvUrl="",hdUrl="",sdUrl="",ldUrl="",
srcUrl="https://video.pearvideo.com/mp4/adshort/20190528/cont-1559965-13958439_adpkg-ad_hd.mp4",vdoUrl=srcUrl,skinRes="//www.pearvideo.com/domain/skin",videoCDN="//video.pearvideo.com";
ex='srcUrl="(.*?)",vdoUrl'
'''
import re

def getiVideoData(url):
    return requests.get(url=url, headers=headers).content
#进行随机保存
def saveVido(data):
    name=str(random.randint(0,9999))+'.mp4'
    with open(name,'wb') as f:
        f.write(data)
    print(name,'下载成功')
for li in li_list:
    detail_url = 'https://www.pearvideo.com/' + li.xpath('./div/a/@href')[0]
    detail_page_text = requests.get(url=detail_url, headers=headers).text
    ex = 'srcUrl="(.*?)",vdoUrl'
    video_src = re.findall(ex, detail_page_text, re.S)[0]  # 正则获取视屏url
    viseo_urls.append(video_src)
print(viseo_urls)
#使用线程池进行视频数据的异步下载
all_video_data_list=pool.map(getiVideoData, viseo_urls)
#保存视频
pool.map(saveVido,all_video_data_list)


import requests
headers = {
    'User-Agent':'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36'
}
session=requests.Session()
#如果这一步产生了cookie,则cookie会被自动存储到session中
session.get(url='https://xueqiu.com/',headers=headers)
#想要对如下的url发送请求,且是携带cookie发动请求
url='https://xueqiu.com/v4/statuses/public_timeline_by_category.json?since_id=-1&max_id=-1&count=10&category=-1'
         #把这里的requests改成session
json_obj = session.get(url=url,headers=headers).json()
print(json_obj)

import os
import requests
from lxml import etree
# start_page=int(input('start page num:'))
# end_page=int(input('end page num:'))

if not  os.path.exists('./daili'):
    os.mkdir('./daili')

headers = {
    'User-Agent':'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36'
}
for i in range(1,3):
    url = 'https://www.xicidaili.com/nn/{}'.format(i)
    response=requests.get(url=url,headers=headers).text
    #实例化
    tree=etree.HTML(response)
    tr_list=tree.xpath('//*[@id="ip_list"]//tr[@class="odd"]')
    # print(tr_list)
    for tr in tr_list:
        one_ip=tr.xpath('.//td[2]/text()')[0]
        port=tr.xpath('.//td[3]/text()')[0]
        list_wr=one_ip+':'+port
        # print(list_wr)
        with open('./ip.txt','a') as f:
            f.write(list_wr+'\n')